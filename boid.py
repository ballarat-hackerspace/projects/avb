import random
import math

from vector import Vector2

class Boid:
    def __init__(self, x, y):
        self.location = Vector2(x, y)
        self.velocity = Vector2(random.uniform(-0.5, 0.5), random.uniform(-0.5, 0.5))
        self.acceleration = Vector2(0, 0)
        self.maxforce = 0.05
        self.maxspeed = 1.5
        self.desiredseparation = 4
        self.neighbordist = 8
        self.enabled = True

    @staticmethod
    def randomf():
        return Boid.mapfloat(random.uniform(0, 255), 0, 255, -0.5, 0.5)

    @staticmethod
    def mapfloat(x, in_min, in_max, out_min, out_max):
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

    def run(self, boids):
        self.flock(boids)
        self.update()

    def update(self):
        self.velocity += self.acceleration
        self.velocity.limit(self.maxspeed)
        self.location += self.velocity
        self.acceleration *= 0

    def applyForce(self, force):
        self.acceleration += force

    def repelForce(self, obstacle, radius):
        futPos = self.location + self.velocity
        dist = obstacle - futPos
        d = dist.mag()

        if d <= radius:
            repelVec = self.location - obstacle
            repelVec.normalize()
            if d != 0:
                repelVec.normalize()
                repelVec *= (self.maxforce * 7)
                if repelVec.mag() < 0:
                    repelVec.y = 0
            self.applyForce(repelVec)

    def flock(self, boids):
        sep = self.separate(boids)
        ali = self.align(boids)
        coh = self.cohesion(boids)

        sep *= 1.5
        ali *= 1.0
        coh *= 1.0

        self.applyForce(sep)
        self.applyForce(ali)
        self.applyForce(coh)

    def separate(self, boids):
        steer = Vector2(0, 0)
        count = 0

        for other in boids:
            if not other.enabled or other == self:
                continue
            d = self.location.dist(other.location)

            if 0 < d < self.desiredseparation:
                diff = self.location - other.location
                diff.normalize()
                diff /= d
                steer += diff
                count += 1

        if count > 0:
            steer /= count

        if steer.mag() > 0:
            steer.normalize()
            steer *= self.maxspeed
            steer -= self.velocity
            steer.limit(self.maxforce)

        return steer

    def align(self, boids):
        sum_vel = Vector2(0, 0)
        count = 0

        for other in boids:
            if not other.enabled or other == self:
                continue
            d = self.location.dist(other.location)

            if 0 < d < self.neighbordist:
                sum_vel += other.velocity
                count += 1

        if count > 0:
            sum_vel /= count
            sum_vel.normalize()
            sum_vel *= self.maxspeed
            steer = sum_vel - self.velocity
            steer.limit(self.maxforce)
            return steer
        else:
            return Vector2(0, 0)

    def cohesion(self, boids):
        sum_loc = Vector2(0, 0)
        count = 0

        for other in boids:
            if not other.enabled or other == self:
                continue
            d = self.location.dist(other.location)

            if 0 < d < self.neighbordist:
                sum_loc += other.location
                count += 1

        if count > 0:
            sum_loc /= count
            return self.seek(sum_loc)
        else:
            return Vector2(0, 0)

    def seek(self, target):
        desired = target - self.location
        desired.normalize()
        desired *= self.maxspeed
        steer = desired - self.velocity
        steer.limit(self.maxforce)
        return steer

    def arrive(self, target):
        desired = target - self.location
        d = desired.mag()
        desired.normalize()

        if d < 4:
            m = Boid.mapfloat(d, 0, 100, 0, self.maxspeed)
            desired *= m
        else:
            desired *= self.maxspeed

        steer = desired - self.velocity
        steer.limit(self.maxforce)
        self.applyForce(steer)

    def wrap_around_borders(self, matrix_width, matrix_height):
        if self.location.x < 0:
            self.location.x = matrix_width - 1
        if self.location.y < 0:
            self.location.y = matrix_height - 1
        if self.location.x >= matrix_width:
            self.location.x = 0
        if self.location.y >= matrix_height:
            self.location.y = 0

    def avoid_borders(self, matrix_width, matrix_height):
        desired = self.velocity

        if self.location.x < 8:
            desired = Vector2(self.maxspeed, self.velocity.y)
        if self.location.x >= matrix_width - 8:
            desired = Vector2(-self.maxspeed, self.velocity.y)
        if self.location.y < 8:
            desired = Vector2(self.velocity.x, self.maxspeed)

