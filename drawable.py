"""
Represents a drawable object that can be displayed on the screen
"""

from canvas import Canvas

class Drawable:
    def __init__(self):
        self.name = ""
        self.canvas = Canvas()

    def isRunnable(self):
        return False

    def isPlaylist(self):
        return False

    def drawFrame(self):
        self.canvas.fillScreen({0, 0, 0})
        return 0

    def setCanvas(self, canvas):
        self.canvas = canvas

    def flipCanvas(self):
        self.canvas.flip()
        return 0

    def start(self):
        pass

    def stop(self):
        pass

