"""
Represents a canvas for drawing to a window
"""

import os
import time

class Canvas():
    def __init__(self):
        super().__init__()
        self.name = "Canvas"
        self.width = 512
        self.height = 64

    def clear(self):
        pass

    def drawPixel(self, x, y, color):
        pass

    def fill(self, color):
        pass

    def flip(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass


def getCanvas():
    if os.environ.get('CANVAS') == 'pygame':
        from canvas_pygame import PygameCanvas as TheCanvas
    else:
        from canvas_matrix import MatrixCanvas as TheCanvas

    return TheCanvas()
