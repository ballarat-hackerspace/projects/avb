#!/usr/bin/env python

import time

from canvas import getCanvas

from pattern_flock import PatternFlock

# create a canvas
canvas = getCanvas()
canvas.start()

pattern = PatternFlock()
pattern.setCanvas(canvas)
pattern.start()

try:
    while True:
        # draw a frame
        pattern.drawFrame()
        canvas.flip()

        time.sleep(0.001)

except KeyboardInterrupt:
    pass

# clean up
canvas.stop()

