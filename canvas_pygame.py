from canvas import Canvas

# virtual hardware
import pygame

class PygameCanvas(Canvas):
    def __init__(self):
        super().__init__()
        self.name = "AvB"

        pygame.init()

        # create a window with size 800x600
        # we'll map the canvas to the physical display later
        self.surface = pygame.display.set_mode((800, 300))
        self.font = pygame.font.SysFont("monospace", 18)

        self.img_front = self.font.render("Front", True, (255, 255, 255))
        self.img_rear = self.font.render("Rear", True, (255, 255, 255))
        self.img_left = self.font.render("Left", True, (255, 255, 255))
        self.img_right = self.font.render("Right", True, (255, 255, 255))

    def clear(self):
        self.surface.fill((0, 0, 0))

    def drawPixel(self, x, y, color):
        self.surface.set_at(mapToVirtual(int(x), int(y)), color)
        return 0

    def fill(self, color):
        self.surface.fill(color)
        return 0

    def flip(self):
        pygame.event.get()
        pygame.display.flip()

        self.clear()

        # prepare for the next frame
        self.drawPlatform()

    def start(self):
        pygame.display.set_caption(self.name)

    def stop(self):
        pygame.quit()
        pass

    def drawPlatform(self):

        # front panel
        self.surface.blit(self.img_front, (176, 75))
        pygame.draw.rect(self.surface, (255, 255, 255), (176, 100, 448, 32), 1)

        # rear panel
        self.surface.blit(self.img_rear, (176, 175))
        pygame.draw.rect(self.surface, (255, 255, 255), (176, 200, 448, 32), 1)

        # left panel
        self.surface.blit(self.img_left, (50, 75))
        pygame.draw.rect(self.surface, (255, 255, 255), (50, 100, 64, 32), 1)

        # right panel
        self.surface.blit(self.img_right, (686, 75))
        pygame.draw.rect(self.surface, (255, 255, 255), (686, 100, 64, 32), 1)

def mapToVirtual(x, y):
    if y < 32:
        if x < 64:
            return (50 + x, 100 + y)
        else:
            return (112 + x, 100 + y)

    else:
        if x < 448:
            return (176 + 448 - x, 200 + 64 - y)

        else:
            return (686 + 512 - x, 100 + 64 - y)
