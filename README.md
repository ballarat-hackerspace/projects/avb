# AvB

# Production
Running on the real hardware requires the rpi-rgb-led-matrix python bindings. See https://github.com/hzeller/rpi-rgb-led-matrix for more information.

```bash
$ ./demo.py
```

# Run locally

Virtual representation of the hardware is provided by pygame.

```bash
$ CANVAS=pygame ./demo.py
```


