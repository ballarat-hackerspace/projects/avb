from canvas import Canvas

# physical hardware
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from rgbmatrix import graphics

class MatrixCanvas(Canvas):
    def __init__(self):
        super().__init__()
        self.name = "AvB"
        options = RGBMatrixOptions()
        options.rows = 32
        options.cols = 64
        options.chain_length = 8
        options.parallel = 2
        options.pwm_bits = 8
        options.gpio_slowdown = 5
        options.brightness = 75

        self.matrix = RGBMatrix(options = options)
        self.canvas = self.matrix.CreateFrameCanvas()

    def clear(self):
        self.canvas.Clear()

    def fill(self, color):
        r, g, b = color
        self.surface.Fill(color)
        return 0

    def drawPixel(self, x, y, color):
        r, g, b = color
        self.canvas.SetPixel(x, y, r, g, b);
            
    def flip(self):
        self.canvas = self.matrix.SwapOnVSync(self.canvas)
        self.clear()

    def start(self):
        pass

    def stop(self):
        pass
