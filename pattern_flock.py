from boid import Boid
from drawable import Drawable
from vector import Vector2

from random import random

MATRIX_WIDTH = 512
MATRIX_HEIGHT = 64

class PatternFlock(Drawable):
    def __init__(self):
        super().__init__()
        self.name = "Flock"
        self.boidCount = 20
        self.boids = []
        for i in range(self.boidCount):
            boid = Boid(MATRIX_WIDTH / 2, MATRIX_HEIGHT / 2)
            boid.maxspeed = 0.380
            boid.maxforce = 0.015

            self.boids.append(boid)

        self.predator = None
        self.wind = Vector2(0, 0)
        self.hue = 0
        self.predatorPresent = False

    def start(self):
        for boid in self.boids:
            boid.maxspeed = 0.380
            boid.maxforce = 0.015

        self.predatorPresent = random() >= 0.5

        if self.predatorPresent:
            self.predator =  Boid(MATRIX_WIDTH / 2, MATRIX_HEIGHT / 3)
            self.predator.maxspeed = 0.385
            self.predator.maxforce = 0.020
            self.predator.neighbordist = 16.0
            self.predator.desiredseparation = 0.0

    def drawFrame(self):
        # effects.DimAll(230)

        apply_wind = random() > 0.980
        if apply_wind:
            self.wind.x = Boid.randomf() * 0.015
            self.wind.y = Boid.randomf() * 0.015

        color = (255, 255, 255) # effects.ColorFromCurrentPalette(self.hue)

        for boid in self.boids:
            if self.predatorPresent:
                boid.repelForce(self.predator.location, 10)

            boid.run(self.boids)
            boid.wrap_around_borders(MATRIX_WIDTH, MATRIX_HEIGHT)
            location = boid.location
            self.canvas.drawPixel(location.x, location.y, color)

            if apply_wind:
                boid.applyForce(self.wind)
                apply_wind = False

        if self.predatorPresent:
            self.predator.run(self.boids)
            self.predator.wrap_around_borders(MATRIX_WIDTH, MATRIX_HEIGHT)

            color = (255, 0, 0) #effects.ColorFromCurrentPalette(self.hue + 128)
            location = self.predator.location
            self.canvas.drawPixel(location.x, location.y, color)

        self.hue += 1

        return 0

