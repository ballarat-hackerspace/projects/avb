import math

"""
Implementation of a 2D vector class with basic operations.
"""

# Example usage:
# vec1 = Vector2(3, 4)
# vec2 = Vector2(1, 2)
# result = vec1 + vec2
# print(result)

class Vector2:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not self.__eq__(other)

    def __add__(self, other):
        return Vector2(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector2(self.x - other.x, self.y - other.y)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self

    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y
        return self

    def __mul__(self, scalar):
        return Vector2(self.x * scalar, self.y * scalar)

    def __truediv__(self, scalar):
        return Vector2(self.x / scalar, self.y / scalar)

    def __imul__(self, scalar):
        self.x *= scalar
        self.y *= scalar
        return self

    def __idiv__(self, scalar):
        self.x /= scalar
        self.y /= scalar
        return self

    def __repr__(self):
        return f"Vector2({self.x}, {self.y})"

    def is_empty(self):
        return self.x == 0 and self.y == 0

    def rotate(self, deg):
        theta = math.radians(deg)
        c = math.cos(theta)
        s = math.sin(theta)
        tx = self.x * c - self.y * s
        ty = self.x * s + self.y * c
        self.x = tx
        self.y = ty

    def normalize(self):
        length = self.length()
        if length == 0:
            return self
        self *= 1.0 / length
        return self

    def dist(self, other):
        d = Vector2(other.x - self.x, other.y - self.y)
        return d.length()

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def mag(self):
        return self.length()

    def mag_sq(self):
        return self.x ** 2 + self.y ** 2

    def truncate(self, length):
        angle = math.atan2(self.y, self.x)
        self.x = length * math.cos(angle)
        self.y = length * math.sin(angle)

    def ortho(self):
        return Vector2(self.y, -self.x)

    @staticmethod
    def dot(v1, v2):
        return v1.x * v2.x + v1.y * v2.y

    @staticmethod
    def cross(v1, v2):
        return v1.x * v2.y - v1.y * v2.x

    def limit(self, maximum):
        mag_sq = self.mag_sq()
        if mag_sq > maximum ** 2:
            self.normalize()
            self *= maximum

