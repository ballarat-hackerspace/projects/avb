import math

PALETTE_CLOUD = ColorPalette([
    (0, 0, 255),
    (0, 0, 139),
    (0, 0, 139),
    (0, 0, 139),
 
    (0, 0, 139),
    (0, 0, 139),
    (0, 0, 139),
    (0, 0, 139),
 
    (0, 0, 255),
    (0, 0, 139),
    (135,206,235),
    (135,206,235),
 
    (173,216,230),
    (255,255,255),
    (173,216,230),
    (135,206,235)
]};

PALETTE_LAVA = ColorPalette([
    (0,0,0),
    (128,0,0),
    (0,0,0),
    (128,0,0),

    (139,0,0),
    (139,0,0),
    (128,0,0),
    (139,0,0),

    (139,0,0),
    (139,0,0),
    (255,0,0),
    (255,165,0),

    (255,255,255),
    (255,165,0),
    (255,0,0),
    (139,0,0)
])

class ColorPalette():
    def __init__(self, colors, size=256):
        if not isinstance(colors, list):
            raise Exception("Must be a list.")

        if size < len(list):
            raise Exception("Palette size is too low.")

        self.colors = colors
        for i in range(size):

        self.csize = len(colors)
        self.size = size

    def get(self, index):
        # get left/right index

        l_index = 

